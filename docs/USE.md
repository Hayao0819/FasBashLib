# Use this library in your script

There are two ways to use this library.

## Stable library Online

See [Release Page](https://github.com/Hayao0819/FasBashLib/releases) to get the code to add into your script.

To update the library version, you should change URL manually.

### Get URL with script

Clone this project and run script

```bash
./tools.sh release link "v0.1.1"
```

## Stable library Offline

See [Release Page](https://github.com/Hayao0819/FasBashLib/releases) and download `fasbashlib.sh`


## The latest dev library Online

Add this line into your script. You should connect to the Internet.

```bash
source /dev/stdin < <(curl -sL "https://raw.githubusercontent.com/Hayao0819/FasBashLib/build-0.1.x/fasbashlib.sh")
```

## The latest dev library Offline

See [Build documemt](./BUILD.md)
